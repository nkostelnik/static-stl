#include <stdio.h>

#include "Array.h"

void testArray() {
		Array<int, 11> arr;

		for (int i = 0; i < 11; i++) {
				arr.pushBack(i);
		}

		for (int& i : arr) {
				printf("%d\n", i);
		}
}

#include "Map.h"

void testMap() {
		Map<int, int, 2> map;
		map.insert(1, 2);
		map.insert(2, 4);

		for (KeyValue<int, int>& kv : map) {
				printf("%d %d\n", kv.key, kv.value);
		}

		for (const KeyValue<int, int>& kv : map) {
				printf("%d %d\n", kv.key, kv.value);
		}
}

#include "String.h"

void testString() {
		String string("Hello World!");
		printf("%s\n", string.cString());
}

int main() {

		testArray();
		testMap();
		testString();

		return 0;
}