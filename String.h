#ifndef STRING_H
#define STRING_H

#include "StdInt.h"

static const uint32_t STRING_DEFAULT_MAX_ELEMENTS = 256;

template<uint32_t Size = STRING_DEFAULT_MAX_ELEMENTS>
class String {

private:

		enum {
				kMaxElements = Size
		};

public:

		String(const char* input) {
				memset(data_, 0, kMaxElements);
				size_t strLength = strlen(input);
				Assert(strLength < kMaxElements, "Attempted to init a string with a character array larger than the specified size of the string");
				memcpy(data_, input, strLength);
		}

public:

		const char* cString() const {
				return data_;
		}

public:

		Iterator<String> begin() {
				return Iterator<String>(data_);
		};

		Iterator<String> end() {
				return Iterator<String>(data_ + size_);
		};

public:

		char data_[kMaxElements];

};

#define String String<>

#endif