#ifndef MAP_H
#define MAP_H

#include "StdInt.h"
#include "Assert.h"
#include "Iterator.h"

template<class A, class B>
class KeyValue {

public:

		KeyValue() { }

		KeyValue(A key_, B value_) 
				: key(key_)
				, value(value_)
		{ }

public:

		A key;
		B value;

};

template<class Key, class Value, uint32_t MaxElements>
class Map {

private:

		enum {
				kMaxElements = MaxElements
		};

public:

		Map() 
				: size_(0) 
		{ }

public:

		Value& operator [] (Key key) {
				for (uint32_t i = 0; i < kMaxElements; ++i) {
						KeyValue<Key, Value>& element = data_[i];
						if (element.key == key) {
								return element.value;
						}
				}
				Assert(false, "Attempted to index a map element that doesn't exist!");
				KeyValue<Key, Value> pair;
				return pair.value;
		}

public:

		void insert(Key key, Value value) {
				KeyValue<Key, Value> pair(key, value);
				Assert(size_ < kMaxElements, "Attempted to insert a new element into a map without enough space free");
				data_[size_] = pair;
				size_++;
		}

public:

		Iterator<KeyValue<Key, Value> > begin() {
				Iterator<KeyValue<Key, Value>> it(data_);
				return it;
		};

		Iterator<KeyValue<Key, Value> > end() {
				Iterator<KeyValue<Key, Value>> it(data_ + size_);
				return it;
		};

private:

		KeyValue<Key, Value> data_[kMaxElements];
		uint32_t size_;

};

#endif