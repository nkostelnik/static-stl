#ifndef ITERATOR_H
#define ITERATOR_H

#include <iterator>

template<class T>
class Iterator {

public:

		typedef Iterator<T> self_type;

		typedef T value_type;

		typedef T& reference;

		typedef T* pointer;

		typedef std::forward_iterator_tag iterator_category;

		typedef int difference_type;

		Iterator(pointer ptr) : ptr_(ptr) { }

		self_type operator++() { self_type i = *this; ptr_++; return i; }

		self_type operator++(int junk) { ptr_++; return *this; }

		reference operator*() { return *ptr_; }

		pointer operator->() { return ptr_; }

		bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }

		bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }

private:

		pointer ptr_;
};

template<class T>
class ConstIterator {

public:

		typedef ConstIterator<T> self_type;

		typedef T value_type;

		typedef T& reference;

		typedef T* pointer;

		typedef int difference_type;

		typedef std::forward_iterator_tag iterator_category;

		ConstIterator(pointer ptr) : ptr_(ptr) { }

		self_type operator++() { self_type i = *this; ptr_++; return i; }

		self_type operator++(int junk) { ptr_++; return *this; }

		const reference operator*() { return *ptr_; }

		const pointer operator->() { return ptr_; }

		bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }

		bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }

private:

		pointer ptr_;
};

#endif