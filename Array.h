#ifndef ARRAY_H
#define ARRAY_H

#include "Assert.h"
#include "Iterator.h"
#include "StdInt.h"

template <typename T, uint32_t MaxElements>
class Array {

private:

		enum {
				kMaxElements = MaxElements
		};

public:

		Array()
				: size_(0)
		{ };

public:

		T& operator [] (int index) {
				T& result = a[index];
				return result;
		}

public:

		void pushBack(T value) {
				Assert(size_ < kMaxElements, "Attempted to push an item onto an array that doesnt have enough space!");
				data_[size_] = value;
				size_++;
		}

public:

		Iterator<T> begin() {
				return Iterator<T>(data_);
		};

		Iterator<T> end() {
				return Iterator<T>(data_ + size_);
		};

private:

		T data_[kMaxElements];
		uint32_t size_;
};

#endif