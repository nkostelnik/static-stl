#ifndef ASSERT_H
#define ASSERT_H

#include <cassert>

#if defined(_DEBUG) || defined(_TEST)  

#define Assert(condition, message) if (!(condition)) { printf("%s:%d %s\n", __FILE__, __LINE__, message); } assert(condition); 

#else

#define Assert(condition, message) (void)0

#endif

#endif